### Bug Description:
```
xxx
xxx
```

### How reproducible: 
100%
### Steps to Reproduce:
```
1. xxx
2. xxx
```
### Actual results:

### Expected results:

### Additional info:
```
** Libvirt XML **:
xxx

**Environment**:
- Version:
- OS (e.g. from /etc/os-release):
- Kernel (e.g. `uname -a`):
- Install tools:
- Others:
```
