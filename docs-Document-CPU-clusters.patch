From 0041b76d5b82a15cbb0b1d6f70fe12e5017b940a Mon Sep 17 00:00:00 2001
From: Andrea Bolognani <abologna@redhat.com>
Date: Mon, 8 Jan 2024 16:11:51 +0100
Subject: [PATCH] docs: Document CPU clusters

Signed-off-by: Andrea Bolognani <abologna@redhat.com>
Reviewed-by: Peter Krempa <pkrempa@redhat.com>
---
 docs/formatcaps.rst   | 26 ++++++++++++++++----------
 docs/formatdomain.rst | 18 ++++++++++--------
 2 files changed, 26 insertions(+), 18 deletions(-)

diff --git a/docs/formatcaps.rst b/docs/formatcaps.rst
index 60f8b7caca..f37532296f 100644
--- a/docs/formatcaps.rst
+++ b/docs/formatcaps.rst
@@ -77,6 +77,12 @@ The ``<host/>`` element consists of the following child elements:
      Note that not all architectures support CPU dies: if the current
      architecture doesn't, the value will be 0 for all CPUs.
 
+   ``cluster_id``
+     Identifier for the cluster the CPU is in.
+
+     Note that not all architectures support CPU clusters: if the current
+     architecture doesn't, the value will be 0 for all CPUs.
+
    ``core_id``
      Identifier for the core the CPU is in.
 
@@ -84,7 +90,7 @@ The ``<host/>`` element consists of the following child elements:
      List of CPUs that are in the same core.
 
      The list will include the current CPU, plus all other CPUs that have the
-     same values for ``socket_id``, ``die_id`` and ``core_id``.
+     same values for ``socket_id``, ``die_id``, ``cluster_id`` and ``core_id``.
 
 ``secmodel``
    To find out default security labels for different security models you need to
@@ -196,7 +202,7 @@ capabilities enabled in the chip and BIOS you will see:
         <microcode version='236'/>
         <signature family='6' model='142' stepping='12'/>
         <counter name='tsc' frequency='2303997000' scaling='no'/>
-        <topology sockets='1' dies='1' cores='4' threads='2'/>
+        <topology sockets='1' dies='1' clusters='1' cores='4' threads='2'/>
         <maxphysaddr mode='emulate' bits='39'/>
         <feature name='ds'/>
         <feature name='acpi'/>
@@ -261,14 +267,14 @@ capabilities enabled in the chip and BIOS you will see:
               <sibling id='0' value='10'/>
             </distances>
             <cpus num='8'>
-              <cpu id='0' socket_id='0' die_id='0' core_id='0' siblings='0,4'/>
-              <cpu id='1' socket_id='0' die_id='0' core_id='1' siblings='1,5'/>
-              <cpu id='2' socket_id='0' die_id='0' core_id='2' siblings='2,6'/>
-              <cpu id='3' socket_id='0' die_id='0' core_id='3' siblings='3,7'/>
-              <cpu id='4' socket_id='0' die_id='0' core_id='0' siblings='0,4'/>
-              <cpu id='5' socket_id='0' die_id='0' core_id='1' siblings='1,5'/>
-              <cpu id='6' socket_id='0' die_id='0' core_id='2' siblings='2,6'/>
-              <cpu id='7' socket_id='0' die_id='0' core_id='3' siblings='3,7'/>
+              <cpu id='0' socket_id='0' die_id='0' cluster_id='0' core_id='0' siblings='0,4'/>
+              <cpu id='1' socket_id='0' die_id='0' cluster_id='0' core_id='1' siblings='1,5'/>
+              <cpu id='2' socket_id='0' die_id='0' cluster_id='0' core_id='2' siblings='2,6'/>
+              <cpu id='3' socket_id='0' die_id='0' cluster_id='0' core_id='3' siblings='3,7'/>
+              <cpu id='4' socket_id='0' die_id='0' cluster_id='0' core_id='0' siblings='0,4'/>
+              <cpu id='5' socket_id='0' die_id='0' cluster_id='0' core_id='1' siblings='1,5'/>
+              <cpu id='6' socket_id='0' die_id='0' cluster_id='0' core_id='2' siblings='2,6'/>
+              <cpu id='7' socket_id='0' die_id='0' cluster_id='0' core_id='3' siblings='3,7'/>
             </cpus>
           </cell>
         </cells>
diff --git a/docs/formatdomain.rst b/docs/formatdomain.rst
index 366918b32c..2d794cc8a2 100644
--- a/docs/formatdomain.rst
+++ b/docs/formatdomain.rst
@@ -1377,7 +1377,7 @@ following collection of elements. :since:`Since 0.7.5`
    <cpu match='exact'>
      <model fallback='allow'>core2duo</model>
      <vendor>Intel</vendor>
-     <topology sockets='1' dies='1' cores='2' threads='1'/>
+     <topology sockets='1' dies='1' clusters='1' cores='2' threads='1'/>
      <cache level='3' mode='emulate'/>
      <maxphysaddr mode='emulate' bits='42'/>
      <feature policy='disable' name='lahf_lm'/>
@@ -1388,7 +1388,7 @@ following collection of elements. :since:`Since 0.7.5`
 
    <cpu mode='host-model'>
      <model fallback='forbid'/>
-     <topology sockets='1' dies='1' cores='2' threads='1'/>
+     <topology sockets='1' dies='1' clusters='1' cores='2' threads='1'/>
    </cpu>
    ...
 
@@ -1414,7 +1414,7 @@ In case no restrictions need to be put on CPU model and its features, a simpler
 
    ...
    <cpu>
-     <topology sockets='1' dies='1' cores='2' threads='1'/>
+     <topology sockets='1' dies='1' clusters='1' cores='2' threads='1'/>
    </cpu>
    ...
 
@@ -1579,12 +1579,14 @@ In case no restrictions need to be put on CPU model and its features, a simpler
 ``topology``
    The ``topology`` element specifies requested topology of virtual CPU provided
    to the guest.
-   Its attributes ``sockets``, ``dies`` (:since:`Since 6.1.0`), ``cores``,
-   and ``threads`` accept non-zero positive integer values.
+   Its attributes ``sockets``, ``dies`` (:since:`Since 6.1.0`), ``clusters``
+   (:since:`Since 10.1.0`), ``cores``, and ``threads`` accept non-zero positive
+   integer values.
    They refer to the total number of CPU sockets, number of dies per socket,
-   number of cores per die, and number of threads per core, respectively.
-   The ``dies`` attribute is optional and will default to 1 if omitted, while
-   the other attributes are all mandatory.
+   number of clusters per die, number of cores per cluster, and number of
+   threads per core, respectively.
+   The ``dies`` and ``clusters`` attributes are optional and will default to 1
+   if omitted, while the other attributes are all mandatory.
    Hypervisors may require that the maximum number of vCPUs specified
    by the ``cpus`` element equals to the number of vcpus resulting from the
    topology.
-- 
2.27.0

